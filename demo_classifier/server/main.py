import logging
import subprocess
import os
import time
import timeit
import shutil
from dotenv import load_dotenv
from datetime import datetime
# from constants import *
from Classifier import NetworkClassifier
from DataExtractorModule import DataExtractorModule
import threading as th
import pandas as pd
import telegram
import random
from elasticsearch import Elasticsearch
from datetime import timezone

keep_going = True
load_dotenv()

def main():
    data_extract = DataExtractorModule()
    classifier = NetworkClassifier()
    df0 = pd.DataFrame()
    # print("List pcap", os.getenv("PATH_PCAP_INPUT"), os.listdir(os.getenv("PATH_PCAP_INPUT")))
    vls = os.listdir(os.getenv("PATH_PCAP_INPUT"))
    for vl in vls:
        pcap_path_in = os.getenv("PATH_PCAP_INPUT") + "/" + vl
        try:
            data_extract.extract_pcap(pcap_path_in)
            data_pre = classifier.predict(os.getenv("csv_path"))
            df = pd.DataFrame(data_pre)
            df0 = df0.append(df)
        except:
            print(f"[!] Có lỗi sảy ra với tệp", vl)
        # Chuyển file pcap sang thư mục out pcap
        pcap_path_out = os.getenv("PATH_PCAP_OUTPUT") + "/" + vl
        os.rename(pcap_path_in, pcap_path_out)
    print("\nall data\n", df0)
    message = ""
    for index, row in df0.iterrows():
        message += "Phát hiện: " + row["pred"] + "| IP " + row["src_ip"] + " Attack => " + row["dst_ip"] + "\n"
    if len(message) > 1:
        send_test_message(message,row["src_ip"])

def send_test_message(message,ip):
    try:
        telegram_notify = telegram.Bot(os.getenv("token_telegram"))    
        telegram_notify.send_message(chat_id=os.getenv("chat_id_telegram"), text=message, parse_mode='Markdown')
    except Exception as ex:
        print(ex)

    try:
        ELASTIC_HOST = "http://14.232.152.36:9200/"
        # es = Elasticsearch(hosts=os.environ.get("ELASTIC_HOST"))
        es = Elasticsearch(hosts=ELASTIC_HOST)
        doc = {
            "description": message,
            "@timestamp": datetime.now(timezone.utc).strftime("%Y-%m-%dT%H:%M:%S"),
            "loai": "classifler",
            "device_id": 0,
            "ip_attack": ip,
        }

        res = es.index(index="dlp_main", document=doc)
        print(res)
    except Exception as ex:
        print(ex)
        logging.error("!!!!!!!!!!! Gửi lên ELK lỗi")



def key_capture_thread():
    global keep_going
    input()
    keep_going = False

def do_stuff():
    th.Thread(target=key_capture_thread, args=(), name='key_capture_thread', daemon=True).start()
    while keep_going:
        time.sleep(3)
        print("\n [XXXX] ENTER ĐỂ DỪNG [XXXX] \n")
        main()
        


if __name__ == '__main__':
    print("Bắt đầu khởi chạy hệ thống")
    print("Thư mục input pcap tại: ", os.getenv("PATH_PCAP_INPUT"))
    print("Thư mục output pcap tại: ", os.getenv("PATH_PCAP_OUTPUT"))
    do_stuff()

# main()