import logging
import subprocess
import os
import pandas as pd
import timeit

import shutil
from shutil import rmtree
from datetime import datetime, time
from cicflowmeter import sniffer
# from constants import *

logger = logging.getLogger(__name__)


class DataExtractorModule():
    def __init__(self):
        self.output_folder = f'log'
        self.counter = 0
        self.limit = 100
        self.csv = pd.DataFrame()
        try:
            if os.path.exists(self.output_folder):
                rmtree(self.output_folder, ignore_errors=True)
            os.mkdir(self.output_folder)
        except Exception as e:
            print(f'{e} Lỗi tạo file log')



    def extract_cic(self, pcap_path):
        # output_path = f'{self.output_folder}/cic.csv'
        output_path = f'log/cic.csv'
        if os.path.exists(output_path):
            os.remove(output_path)
        # cmd = f'cicflowmeter -f {pcap_path} -c {output_path}'
        # p = subprocess.Popen(
        #     cmd, shell=True)
        # p.wait()
        s = sniffer.create_sniffer(
            input_interface=None,
            input_file=pcap_path,
            output_file=output_path,
            output_mode="flow"
        )
        s.start()
        try:
            s.join()
        except Exception as e:
            logger.error(f'{e} Lỗi extract CIC')
            s.stop()
        finally:
            s.join()
        if os.path.exists(output_path) and os.stat(output_path).st_size:
            df = pd.read_csv(output_path)
            self.csv = pd.concat([self.csv, df])
            self.counter += 1
            logger.info(self.counter)
            if self.counter == self.limit:
                self.csv.to_csv(output_path+"batch.csv")
                self.counter = 0
                self.csv = pd.DataFrame()
            return output_path
        else:
            return None

    def extract_pcap(self, pcap_path):
        pcapfix_path = 'tools/pcapfix/pcapfix'


        cmd = f'{pcapfix_path} -o {pcap_path} {pcap_path}'
        p = subprocess.Popen(
            cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.wait()

        # Extract files from pcap file
        output = self.extract_cic(pcap_path)
        # extract_pcap.delay(pcap_path)

        # extract_pcap.delay(file_pcap)
        # print("!!!!!!!!!!!", output)
        return output

# p1 = DataExtractorModule()
# p1.extract_pcap()