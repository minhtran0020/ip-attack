import logging
import subprocess
import os
import time
import timeit
import shutil
from dotenv import load_dotenv
from datetime import datetime
from constants import *
from Classifier import NetworkClassifier
from DataExtractorModule import DataExtractorModule
import threading as th

keep_going = True
load_dotenv()

def main():
    data_extract = DataExtractorModule()
    classifier = NetworkClassifier()
    data_predict = []

    # print("List pcap", os.getenv("PATH_PCAP_INPUT"), os.listdir(os.getenv("PATH_PCAP_INPUT")))
    vls = os.listdir(os.getenv("PATH_PCAP_INPUT"))
    for vl in vls:
        pcap_path_in = os.getenv("PATH_PCAP_INPUT") + "/" + vl
        try:
            data_extract.extract_pcap(pcap_path_in)
            data_pre = classifier.predict(os.getenv("csv_path"))
            # data_predict.append(data.get('pred'))
            for data in data_pre:
                
                data_predict.append(data.append(data))

            # data_predict.append(data['pred'].values)
            print(data, "|", data.get('pred'))
        except:
            print(f"[!] Có lỗi sảy ra với tệp", vl)
        # Chuyển file pcap sang thư mục out pcap
        pcap_path_out = os.getenv("PATH_PCAP_OUTPUT") + "/" + vl
        os.rename(pcap_path_in, pcap_path_out)
    # print("\nall data", data_predict)
    for i in data_predict:
        print("???", i)



def key_capture_thread():
    global keep_going
    input()
    keep_going = False

def do_stuff():
    th.Thread(target=key_capture_thread, args=(), name='key_capture_thread', daemon=True).start()
    while keep_going:
        time.sleep(2)
        print("\n [XXXX] ENTER ĐỂ DỪNG [XXXX] \n")
        main()
        


# if __name__ == '__main__':
#     print("Bắt đầu khởi chạy hệ thống")
#     print("Thư mục input pcap tại: ", os.getenv("PATH_PCAP_INPUT"))
#     print("Thư mục output pcap tại: ", os.getenv("PATH_PCAP_OUTPUT"))
#     do_stuff()

main()